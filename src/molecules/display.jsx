import React from "react";

import "./display.css";

const Display = ({value}) => {
    return (
      <div className="component-display">
        <div>{value}</div>
      </div>
    );
  }

  export default Display
