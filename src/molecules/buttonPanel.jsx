import React from "react";
import Button from "../atoms/button";

import "./buttonPanel.css";

const ButtonPanel = ({handleClick}) => {
    return (
      <div className="component-button-panel">
        <div>
          <Button text="7" onClick={() => handleClick("7")} />
          <Button text="8" onClick={() => handleClick("8")} />
          <Button text="9" onClick={() => handleClick("9")} />
        </div>
        <div>
          <Button text="4" onClick={() => handleClick("4")} />
          <Button text="5" onClick={() => handleClick("5")} />
          <Button text="6" onClick={() => handleClick("6")} />
        </div>
        <div>
          <Button text="1" onClick={() => handleClick("1")} />
          <Button text="2" onClick={() => handleClick("2")} />
          <Button text="3" onClick={() => handleClick("3")} />
        </div>
        <div>
          <Button text="0" onClick={() => handleClick("0")} />
        </div>
      </div>
    );
  }

  export default ButtonPanel
