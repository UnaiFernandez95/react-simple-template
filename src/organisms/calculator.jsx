import React, { useState } from "react"
import ButtonPanel from "../molecules/buttonPanel"
import Display from "../molecules/display"

import "./calculator.css"

const Calculator = () => {
  const [value, setValue] = useState(0)
  const handleClick = (number) => {
    setValue(number)
  }
  return (
    <div className="calculator">
    <Display value={value}/>
    <ButtonPanel handleClick={handleClick} />
    </div>
  )
}

export default Calculator
