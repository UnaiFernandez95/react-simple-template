# React 

## Conceptos Iniciales 

React es una **libreria** de JavaScript que nos agiliza el trabajo a la hora de crear apps en front.

### Libreria VS FrameWork
Un **Framework** te da una arquitectura u orden de carpetas y un conjunto de reglas que facilita el desarrollo de aplicaciones. 
+ funcionalidades
+ rapidez a la hora de construir 

Una **libreria** conjunto de funciones y componentes reutilizables que nos ayudan a crear mas rapida la app sin acoplarnos a ella.

+ no dicta una arquitectura
+ ofrecer soluciones para problemas específicos
+ MÁS LIBERTAD 

En este proyecto sencillo utilizamos react como libreria junto con otras. Pero tambien Existen frameworks basadas en react como: Remix, Gatsby, Expo (para aplicaciones nativas (para moviles))

### Diccionario React 
**Componentes**: un componente es una parte independiente y reusable de la interfaz de usuario, una pequeña parte de la pagina web que montemos o una pagina independiente. 

**Props**: esta es una abreviación de "propiedades.". Son una especie de "argumentos" que pasan de un componente a otro.

**Estado**: el estado de un componente es una representación de su conjunto de propiedades y sus valores actuales en un momento en particular de la ejecución de la aplicación. Los componentes pueden manejar y actualizar su estado internamente.

**Hook**: "funciones" especificas de react para montar la aplicacion o puedes crear tus propios "hooks"


### Documentación 

Documentación oficial React: https://es.react.dev/learn

Arquitectura atomic design:  https://www.crehana.com/blog/transformacion-digital/que-es-atomic-design/ 

Frameworks React: 

- Solo web:
Remix :  https://remix.run/
Next.js: https://nextjs.org/docs

- Nativas (web y movil):
Expo: https://expo.dev/


